import sqlite3 as sql
import hashlib
import string
from random import randint
from Core.Command_Exe import CommandManager


def hashpw(pw):
    rhash = hashlib.sha3_512(pw.encode("utf-8")).hexdigest()
    return rhash


def compare_pw(username, pw, database):
    data = database.get_info(username)
    try:
        pw1 = data[1]
        pw2 = hashpw(pw)
        if pw1 == pw2:
            return True
        else:
            return False
    except:
        return False


def generate_token(lengh):
    s = ""
    letters = list(string.ascii_letters)
    for x in range(0, lengh):
        s += letters[randint(0, len(letters) - 1)]
    return s


class User:
    def __init__(self, name, token, command_manager):
        self.name = name

        self.token = token

        self.command_manager = command_manager

    def execute(self, command):
        return self.command_manager.execute(self.token, self.name, command)


class UserManagment:
    def __init__(self, head):

        self.user_by_name = {}
        self.user_by_token = {}
        self.head = head
        self.command_manager = CommandManager(self.head)

        self.userdb = Database("./Core/Data/User.sqlite")

        if self.userdb.get_info("root") is None:
            self.userdb.add_user("root", hashpw("root"))

    def login(self, username, password):
        if compare_pw(username, password, self.userdb) is True:
            token = generate_token(32)
            self.user_by_token.update({token: User(username, token, self.command_manager)})
            return token
        return False

    def forward(self, token, command):
        if token in self.user_by_token:
            return self.user_by_token[token].execute(command)
        else:
            return "Unauthorized invalid token"

    def logout(self, token):
        del (self.user_by_token[token])

    def check_token(self, token, username):
        if token in self.user_by_token:
            if self.user_by_token[token].name == username:
                return True
        return False

    def change_pw(self, username, token, new):

        if self.check_token(token, username) is True:
            self.userdb.update(username, hashpw(new))
            return "Successful changed password"
        return "check username, password or you token"


class Database:
    def __init__(self, path):
        self.connection = sql.connect(path)
        self.cursor = self.connection.cursor()

    def add_user(self, username, pw):
        try:
            sql_command = "CREATE TABLE " + str(username) + " ( user TEXT , pw TEXT )"
            self.cursor.execute(sql_command)
            self.connection.commit()
            self.insert(username, pw)
        except:
            print("USER TABLE ALREADY EXSITS")

    def insert(self, username, pw):
        command = "INSERT INTO " + str(username) + " VALUES( ? , ? )"
        self.cursor.execute(command, (str(username), str(pw)))
        self.connection.commit()

    def update(self, name, pw):
        command = "UPDATE " + str(name) + " SET pw = ? "
        self.cursor.execute(command, (pw,))
        self.connection.commit()

    def get_info(self, username):  # get data from database and returns them
        try:
            self.cursor.execute("SELECT * FROM " + str(username))
            mr = self.cursor.fetchall()[0]
            return mr
        except:
            return None
