import datetime


class Logger:
    def __init__(self):
        self.file_errors = open("./Logs/errors.log", "a+")
        self.file_info = open("./Logs/info.log", "a+")
        self.file_module = open("./Logs/module.log", "a+")
        self.log = []

    def write(self, text, type="INFO", module = False):
        time = datetime.datetime.now()
        self.log.insert(0, [type, time, text])

        if module is True:
            self.file_module.write("[" + type + "] " + str(time) + " " + text + "\n")

        elif type == "ERROR" or type == "CRITICAL":
            self.file_errors.write("[" + type + "] " + str(time) + " " + text + "\n")

        else:
            self.file_info.write("[" + type + "] " + str(time) + " " + text + "\n")

    def get_last_log(self, size):
        try:
            return self.log[0:size]
        except:
            return self.log[0:len(self.log)]

    def __del__(self):
        self.file_errors.close()
        self.file_info.close()
        self.file_module.close()