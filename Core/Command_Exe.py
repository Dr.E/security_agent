import sys


def command_parse(command):
    temp = command.split(" ")[1:]
    string = ""
    isopen = False
    return_value = []
    for el in temp:
        if el[0] == '"':
            isopen = True
            string += el[1:]
        elif '"' in el[len(el) - 3:]:
            string += el[:len(el) - 1]
            return_value += [string]
            isopen = False
        elif isopen is True:
            string += " " + el + " "
        else:
            return_value += [el]
    return return_value


class CommandManager:
    def __init__(self, head):
        self.head = head
        self.other_commands = []

    def execute(self, token, username, command):

        self.head.logger.write("info", command)

        if command == "logout":
            self.head.user_manager.logout(token)
            return "successful logout"

        elif command[0:3] == "cpw":
            password = command[4:]
            self.head.user_manager.change_pw(username, token, password)
            return "successful changed password"

        elif command == "packages":
            return self.head.comhub.get_packages()

        elif command[0:11] == "kick_module":
            status = self.head.comhub.kick_module(command[12:])
            return status

        elif command[0:12] == "start_module":
            return self.head.comhub.start_module(command[13:])

        elif command[0:13] == "update_module":
            command = command[14:].split(" ")
            if "-a" in command:
                self.head.comhub.update()
                return "successful update all modules"
            else:
                self.head.comhub.update_single(command[0])
                return "successful update single module"

        elif command == "commands":
            ret = ""
            for command in self.other_commands:
                ret += command.__name__ + "\n"
            return ret



        else:
            for excommand in self.other_commands:

                self.head.logger.write(str(excommand.__self__.__class__) + " " + excommand.__name__)

                if excommand.__name__ == command.split(" ")[0]:
                    try:
                        ret = excommand()

                    except:

                        command = command_parse(command)

                        ret = excommand(command)
                    return ret
            self.head.logger.write(command)
            return "Unknown Command"
