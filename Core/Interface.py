import Core.User_Ident as UserIdent


class Interface:
    def __init__(self, head):
        self._head = head
        self.module_messages = []

    def login(self, username, password):
        return self._head.user_manager.login(username, password)

    def execute(self, token, command):
        return self._head.user_manager.forward(token, command)

    def logout(self, token):
        self._head.user_manager.logout(token)

    def get_log(self, token, size=5):
        if token == self._head.token:
            data = self._head.logger.get_last_log(size)
            return data
        else:
            return "Unauthorized invalid token"

    def get_module_messages(self, token):
        if token in self._head.user_manager.user_by_token:
            message, self.module_messages = "\n".join(self.module_messages), []
            return message
        return "Unauthoried invalid token"
