from Core.Module_Loader import *
from _thread import start_new_thread
import os


class CommunicationHub:
    def __init__(self, head):
        self._head = head

    def _update(self):
        self._packages = import_modules(self)

    def _load_module_functions(self):
        functions = []
        for package in self._packages:
            if hasattr(self._packages[package], "functions") is True:
                functions += self._packages[package].functions
        return functions

    def _delete_functions(self, functions, package):
        old_functions = functions
        if package in self._packages:
            if hasattr(self._packages[package], "functions") is True:
                for func in self._packages[package].functions:
                    if func in old_functions:
                        del (functions[functions.index(func)])
            return functions

    def log_event(self, status, event):
        self._head.logger.write(event, status, True)

    def update_single(self, name):

        if name in self._packages:
            self.kick_module(name)
        if os.path.exists("./Module/" + name + ".py"):
            obj = import_module("./Module/" + name + ".py", self)
        else:
            obj = import_module("./Module/" + name, self)
        self._packages.update({name: obj})
        if hasattr(self._packages[name], "functions") is True:
            functions = self._load_module_functions()
            self._head.user_manager.command_manager.other_commands = functions

    def send(self, receiver, transmitter, message):
        if receiver in self._packages:
            try:
                self._packages[receiver].receive(transmitter, message)
            except:
                self.send2user("An Error has occurred in package: " + receiver)
        else:
            self._packages[transmitter].receive("COMHUB", "Unknown module: " + receiver)

    def start_module(self, name):
        try:
            start_new_thread(self._packages[name].start, ())
            return "Successful started module"
        except:
            return "Unknown module: " + name

    def kick_module(self, name):
        if name in self._packages:

            functions = self._head.user_manager.command_manager.other_commands
            functions = self._delete_functions(functions, name)
            self._head.user_manager.command_manager.other_commands = functions

            try:
                self._packages[name].__del__()
            finally:
                del (self._packages[name])
        else:
            return "Module with name " + name + " not found"

    def send2user(self, message):
        self._head.interface.module_messages += [message]

    def get_packages(self, as_list=False):
        if as_list is False:
            return "\n".join(self._packages)
        else:
            return self._packages
