import os
from imp import load_source, load_compiled
import inspect


def import_module(filepath, interface):
    class_inst = None

    mod_name, file_ext = os.path.splitext(os.path.split(filepath)[-1])
    print(mod_name)
    if file_ext.lower() == '.py':
        py_mod = load_source(mod_name, filepath)

    elif file_ext.lower() == '.pyc':
        py_mod = load_compiled(mod_name, filepath)

    else:  # Allows to build bigger Modules
        files = os.listdir(filepath)
        for file in files:

            if "__init__.py" == file or "main.py" == file or "start.py" == file:
                filepath = filepath + "/" + file
                mod_name = file[:3]
                py_mod = load_source(mod_name, filepath)

            elif "__init__.pyc" == file or "main.pyc" == file or "start.pyc" == file:
                filepath = filepath + "/" + file
                mod_name = file[:4]
                py_mod = load_compiled(mod_name, filepath)

    if ".py" in filepath and ".pyc" not in filepath:
        expected_class = sort_class_name(filepath)

    else:
        for name, obj in inspect.getmembers(py_mod):
            if inspect.isclass(obj):
                expected_class = name
                break

    if hasattr(py_mod, expected_class):
        class_inst = getattr(py_mod, expected_class)(interface)

    return class_inst


def sort_class_name(file):
    if ".py" in file and ".pyc" not in file:
        d = open(file)
        text = d.readlines()
        d.close()
        for line in text:
            if "class" in line:
                index = line.index("class")
                if "#" not in line[:index]:
                    name = line[index + 6:].split(":")
                    return name[0]


def import_modules(interface):
    modules = os.listdir("./Module/")
    objects = {}
    for module in modules:
        if module != "__pycache__" and module != ".git":
            obj = import_module("./Module/" + module, interface)
            if ".py" in module:
                objects.update({module[:len(module) - 3]: obj})
            else:
                objects.update({module: obj})
    print(objects)
    return objects
