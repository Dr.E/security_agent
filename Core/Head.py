from Core.Logger import Logger
import Core.User_Ident as UserIdent
from Core.Interface import Interface
from Core.Command_Exe import CommandManager
from Core.Communication_Hub import CommunicationHub


class SecurityAgent:
    def __init__(self):

        self.logger = Logger()

        self.logger.write("security agent initiate")

        self.user_manager = UserIdent.UserManagment(self)

        self.logger.write("setup UserManager ... success")

        self.interface = Interface(self)

        self.logger.write("initiate Interface ... succses")

        self.comhub = CommunicationHub(self)

        self.logger.write("initiate Communication Hub ... success")

        self.comhub._update()

        self.logger.write("load modules ... success")

        self.user_manager.command_manager.other_commands = self.comhub._load_module_functions()

        self.logger.write("load external commands ... success")

    def return_interface(self):
        return self.interface
