from gtts import gTTS
import os


def say(text):
    tts = gTTS(text=text, lang="en")
    tts.save("./temp/say.mp3")
    os.system("mpg123 -q ./temp/say.mp3")
