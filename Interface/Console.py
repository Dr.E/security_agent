import curses
import datetime
import time
from _thread import start_new_thread


class Console:
    def __init__(self, core_interface):

        self.core = core_interface

        self.screen = curses.initscr()
        self.height, self.width = self.screen.getmaxyx()

        self.screen.keypad(1)

        self.console = self.screen.subwin(self.height - 5, int(self.width / 2 - 2), 2, 1)
        self.info = self.screen.subwin(self.height - 5, int(self.width / 2 - 2), 2, int(self.width / 2))
        self.input_frame = self.screen.subwin(1, self.width, self.height - 2, 0)
        self.input_frame.addstr(0, 0, ">>>")
        self.console.refresh()
        self.info.refresh()
        self.input_frame.refresh()

        self.message_console_count = 1
        self.message_console = []
        self.message_info_count = 1
        self.message_info = []

        self.screen.keypad(True)

        self.setup_interface()

        self.screen.refresh()

    def setup_interface(self):
        # self.clear()

        self.screen.addstr(0, 0, "Room Control V 0.0.4")

        self.screen.addstr(1, 0, "Console:")

        self.screen.addstr(1, int(self.width / 2), "Sys Output:")

        self.console.border('|', '|', '-', '-', '+', '+', '+', '+')
        self.info.border('|', '|', '-', '-', '+', '+', '+', '+')

    def clear(self):
        self.screen.clear()

    def split_message(self, message, size):
        temp_value = message.split("\n")
        return_value = []

        count = 0

        for message in temp_value:

            rotations = int(len(message) / size)

            for i in range(rotations + 1):
                try:
                    return_value += [message[i * size:(i + 1) * size]]
                except:
                    return_value += [message[i * size:]]

            count += 1

        return return_value

    def add2console(self, message):

        if type(message) is not str:
            return

        height, width = self.console.getmaxyx()

        self.message_console += self.split_message(message, width - 2)

        self.screen.addstr(self.height - 1, 0, str(len(self.message_console)) + " / " + str(height))
        try:
            self.console.clear()
            for line, message in enumerate(self.message_console):
                self.console.addstr(line + 1, 1, message)

        except:
            self.console.clear()
            for line, message in enumerate(self.message_console[0 - height:-2]):
                self.console.addstr(line + 1, 1, message)

        self.console.border('|', '|', '-', '-', '+', '+', '+', '+')
        self.console.refresh()
        self.screen.refresh()

    def add2output(self, message):

        if type(message) is not str:
            return

        height, width = self.info.getmaxyx()

        self.message_info += self.split_message(message, width - 2)

        self.screen.addstr(self.height - 1, 0, str(len(self.message_console)) + " / " + str(height))
        try:
            self.info.clear()
            for line, message in enumerate(self.message_info):
                self.info.addstr(line + 1, 1, message)

        except:
            self.info.clear()
            for line, message in enumerate(self.message_info[0 - height:-2]):
                self.info.addstr(line + 1, 1, message)

        self.info.border('|', '|', '-', '-', '+', '+', '+', '+')
        self.info.refresh()
        self.screen.refresh()

    def input(self):

        x = self.input_frame.getstr(0, 3).decode("utf-8")
        self.input_frame.addstr(0, 3, (self.width - 4) * " ")
        return x

    def render(self):

        while True:

            date = datetime.datetime.now()

            module_messages = self.core.get_module_messages(self.token)

            if module_messages != "":
                self.add2output(module_messages)

            self.screen.addstr(0, self.width - 30, str(date))

            self.screen.refresh()
            self.console.refresh()
            self.info.refresh()
            self.input_frame.refresh()
            time.sleep(1)

    def login(self):
        self.token = self.core.login("root", "root")

    def loop(self):
        self.login()

        start_new_thread(self.render, ())

        while True:
            command = self.input()
            if command == "exit":
                break
            text = self.core.execute(self.token, command)

            self.add2console(text)

            if text == "Unauthorized invalid token":
                self.login()

            self.screen.refresh()
            self.console.refresh()
            self.info.refresh()
            self.input_frame.refresh()
